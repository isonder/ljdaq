import asyncio
from asyncio import Event
import numpy
from traitlets.config import Config, Application
from enum import Enum
import time
import signal

from ljdaq import IOConf
from ljdaq.dev_io import LabjackT7, LabjackT7Backend
from ljdaq.writers import TextWriter


class LabjackStreamedIo(Application):
    """
    """

    classes = [IOConf, LabjackT7, LabjackT7Backend, TextWriter]

    def __init__(self, **kwds):
        super().__init__(**kwds)
        self.dev = LabjackT7(name="GHZ_T7_0", serial=470012431, **kwds)
        self.loop = asyncio.get_event_loop()
        self.stop_received: Event = Event()
        self.generate_config_file()
        for sig in (signal.SIGHUP, signal.SIGTERM, signal.SIGINT):
            self.loop.add_signal_handler(
                sig, self.sig_handler, sig, self.stop_received)

    @staticmethod
    def sig_handler(sig: Enum, evt: Event):
        print(f"LabjackStreamedIo.sig_handler: Got {sig.__repr__()}.")
        evt.set()

    def start(self):
        self.dev.connect(self.dev.conn_type)
        try:
            self.loop.run_until_complete(self.run())
        finally:
            self.loop.close()
            print("LabjackStreamedIo: Done.")

    async def run(self):
        que = asyncio.Queue()
        tasks = [
            self.produce(que),
            self.consume(que)
        ]
        try:
            await asyncio.gather(*tasks)
            print(f"LabjackStreamedIo.run: tasks done.")
            self._cleanup()
        except asyncio.CancelledError:
            pass

    @staticmethod
    def _cleanup():
        pass

    async def produce(self, que: asyncio.Queue):
        self.dev.start()
        print(f"LabjackStreamedIo.produce: started {self.dev}.")
        while not self.stop_received.is_set():
            asyncio.create_task(que.put(await asyncio.to_thread(self.dev.read)))
        asyncio.create_task(que.put(asyncio.to_thread(self.dev.stop)))
        print(f"LabjackStreamedIo.produce: {self.dev} is done.")
        await que.put(None)

    async def consume(self, que: asyncio.Queue):
        wrtr = TextWriter(config=self.config)
        wrtr.write_config(self.config)
        wrtr.open()
        while 1:
            data = await que.get()
            if data is not None:
                asyncio.create_task(asyncio.to_thread(wrtr.write, data))
            else:
                asyncio.create_task(asyncio.to_thread(wrtr.close))
                print(f"LabjackStreamedIo.consume: done.")
                break


if __name__ == "__main__":
    c = Config()

    c.Writer.record = "test"
    c.Writer.part = "t7"
    c.IOConf.names = ["time", "something"]
    c.IOConf.descs = ["s", "V"]
    c.IOConf.start_time = time.time()
    c.LabjackT7Backend.srate = 1000.

    c.TextWriter.target = "test.txt"
    # t7 = LabjackT7(name="GHZ_T7_0", serial=470012431, config=c)
    # t7.backend.srate = 1000.
    # print(t7)
    # print(f"{t7.backend=},\n  {t7.backend.srate=}")

    app = LabjackStreamedIo(config=c)
    # app = LabjackStreamedIo.launch_instance(device=t7, config=c)
    print(
        f"{app.dev=}\n"
        f"  {app.dev.backend=}\n"
        f"  {app.dev.backend.srate=}\n"
        f"  {app.dev.start_time=}"
    )
    app.start()
