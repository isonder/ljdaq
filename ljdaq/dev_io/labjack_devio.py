import time
from labjack import ljm
from labjack.ljm import constants as ljc
import traitlets as trlt
import traitlets_paths as tpth
import re
import json
import sys
from typing import Union, Callable
from warnings import warn

from ljdaq import LJM_CONST_PATH, IOConf
from ljdaq.dev_io import Device, DeviceBackend, DebugPort

LJC_UINT16 = ljc.UINT16
LJC_UINT32 = ljc.UINT32
LJC_INT32 = ljc.INT32
LJC_FLOAT32 = ljc.FLOAT32
LJC_BYTE = ljc.BYTE
LJC_STRING = ljc.STRING


class LabjackRegisterBase(trlt.HasTraits):
    """The RegisterBase has traits that are common for Registers and
    RegisterGroups.
    """
    name = trlt.Unicode(default_value='', help="The register's name.")
    address = trlt.Int(default_value=-1)
    dtype = trlt.Unicode(default_value="invalid", help="The data type.")
    access = trlt.Unicode(
        default_value="none", help="Read-, write-, or read-write access.")
    streamable = trlt.Bool(default_value=False)
    altnames = trlt.List(default_value=[])
    constants = trlt.List(default_value=[])
    default = trlt.Any(default_value=None)
    uses_ram = trlt.Bool(default_value=False)
    is_buffer = trlt.Bool(default_value=False)
    tags = trlt.List(default_value=[])
    desc = trlt.Unicode(default_value='')
    _dtcode = trlt.Int(default_value=-1)
    DTMAP = {
        'uint16': LJC_UINT16,  'uint32':  LJC_UINT32,
        'int32':  LJC_INT32,   'i32':     LJC_INT32,
        'f32':    LJC_FLOAT32, 'float32': LJC_FLOAT32,
        'string': LJC_STRING,  'str':     LJC_STRING,
        'bytes':  LJC_BYTE,    'byte':    LJC_BYTE,
        'uint64': -1,          'invalid': -1
    }
    _WS_PATTERN = re.compile(r'\s+')  # White space regex pattern
    ACCESSTYPES = ('r', 'w', 'rw')
    # numeric data types handled by a labjack.
    NUM_LJC_TYPES = [LJC_UINT16, LJC_UINT32, LJC_INT32, LJC_FLOAT32]
    MAX_ADDRESS = 65_535              # 2 ** 16 - 1
    FORMAT_LINE_WIDTH = 80            # This could also become a configurable.

    @trlt.validate('address')
    def _validate_address(self, proposal):
        val = int(proposal['value'])
        if val < -1 or val > self.MAX_ADDRESS:
            raise trlt.TraitError(
                f"Received a wrong address: {val}. A valid modbus register "
                f"address must be a value between 0 and {self.MAX_ADDRESS}.")
        return val

    @trlt.validate('dtype')
    def _validate_dtype(self, proposal):
        ret = proposal['value'].lower()
        if ret not in self.DTMAP.keys():
            raise trlt.TraitError(f"Got a wrong value for dtype: '{ret}'. "
                                  f"Expected one of {self.DTMAP.keys()}")
        self._dtcode = self.DTMAP[ret]
        return ret

    @property
    def dtcode(self) -> int:
        return self._dtcode

    @trlt.validate('access')
    def _validate_access(self, proposal):
        if proposal['value'].lower() not in self.ACCESSTYPES:
            raise trlt.TraitError(
                f"Got a wrong value for access: '{proposal['value']}'. "
                f"Expected one of {self.ACCESSTYPES}.")
        return proposal['value'].lower()

    @classmethod
    def format_desc(cls, desc: str) -> str:
        ln, ret = len(desc), ""
        if ln > 0:
            tmp = desc.split(" ")
            s, line = [], ""
            for word in tmp:
                if len(line) + len(word) < cls.FORMAT_LINE_WIDTH:
                    line += " " + word
                else:
                    s.append(line[1:])
                    line = ""
            s.append(line[1:])
            ret += "\n".join(s)
        return ret


class LabjackRegisterError(Exception):
    method_name: str = ""
    missing_phrase: dict = {
        'handle': 'Device handle not given.',
        'lng': f'Array length to {method_name} not given.'
    }

    def __init__(self, name: str, addr: int, what: str, message: str = ""):
        self.name = name
        msg = "" if message == "" else message + '\n'
        msg += f"{name}.{self.method_name}(): {self.missing_phrase[what]}."
        msg += f"({name}: {addr})"
        super(LabjackRegisterError, self).__init__(msg)


class LabjackRegReadError(LabjackRegisterError):
    method_name = "read"


class LabjackRegWriteError(LabjackRegisterError):
    method_name = "write"


class LabjackRegister(LabjackRegisterBase):
    """A modbus register. Includes `read()` and `write()` methods which are
    aware of their own data type.
    """

    @classmethod
    def reader_factory(cls, dtc: int, addr: int, buf: bool = False) -> Callable:
        if dtc in cls.NUM_LJC_TYPES:
            if buf:
                def read(handle: int, lng: int):
                    if handle is None or lng is None:
                        what = "handle" if handle is None else "lng"
                        raise LabjackRegReadError(cls.name, addr, what)
                    return ljm.eReadAddressArray(handle, addr, dtc, lng)
            else:
                def read(handle: int):
                    if handle is None:
                        raise LabjackRegReadError(cls.name, addr, "handle")
                    return ljm.eReadAddress(handle, addr, dtc)
        elif dtc == LJC_BYTE:
            def read(handle: int, lng: int):
                if handle is None or lng is None:
                    what = "handle" if handle is None else "lng"
                    raise LabjackRegReadError(cls.name, addr, what)
                return ljm.eReadAddressByteArray(handle, addr, lng)
        elif dtc == LJC_STRING:
            def read(handle: int):
                if handle is None:
                    raise LabjackRegReadError(cls.name, addr, "handle")
                return ljm.eReadAddressString(handle, addr)
        else:
            raise TypeError(f"'{dtc}' is not a valid type for a "
                            f"LabjackRegister's read method.")
        return read

    @classmethod
    def writer_factory(cls, dtc: int, addr: int, buf: bool = False) -> Callable:
        if dtc in cls.NUM_LJC_TYPES:
            if buf:
                def write(handle: int, val):
                    if handle is None or val is None:
                        what = "handle" if handle is None else "val"
                        raise LabjackRegWriteError(cls.name, addr, what)
                    ljm.eWriteAddressArray(handle, addr, dtc, len(val), val)
            else:
                def write(handle: int, val):
                    if handle is None or val is None:
                        what = "handle" if handle is None else "val"
                        raise LabjackRegWriteError(cls.name, addr, what)
                    ljm.eWriteAddress(handle, addr, dtc, val)
        elif dtc == LJC_BYTE:
            def write(handle, val):
                if handle is None or val is None:
                    what = "handle" if handle is None else "val"
                    raise LabjackRegWriteError(cls.name, addr, what)
                ljm.eWriteAddressByteArray(handle, addr, len(val), val)
        elif dtc == LJC_STRING:
            def write(handle: int, val):
                if handle is None or val is None:
                    what = "handle" if handle is None else "val"
                    raise LabjackRegWriteError(cls.name, addr, what)
                ljm.eWriteAddressString(handle, addr, val)
        else:
            raise TypeError(f"'{dtc}' is not a valid type for a "
                            f"LabjackRegister's write method.")
        return write

    @trlt.validate('name')
    def _validate_name(self, proposal):
        ret = proposal['value']
        if len(ret) > ljc.MAX_NAME_SIZE:
            raise trlt.TraitError(
                f"Modbus register names cannot be longer than "
                f"{ljc.MAX_NAME_SIZE} characters. Got a request to set a name "
                f"with {len(ret)} characters."
            )
        ret = re.sub(self._WS_PATTERN, "", ret)
        return ret.upper()

    @trlt.observe('address', 'dtype', 'access', 'is_buffer')
    def _addr_or_dtc_changed(self, change):
        """Create new reader and writer methods if necessary.
        """
        nm = change['name']
        addr = change['new'] if nm == 'address' else self.address
        dtc = self.DTMAP[change['new']] if nm == 'dtype' else self.dtcode
        acc = change['new'] if nm == 'access' else self.access
        if 'r' in acc:
            # print(f"creating new reader method. owner: {change['owner']}")
            self.read = self.reader_factory(dtc, addr, False)
            if self.is_buffer and self.dtcode in self.NUM_LJC_TYPES:
                self.read_arr = self.reader_factory(dtc, addr, True)
        else:
            self.read = None
        if 'w' in acc:
            # print(f"creating new writer method. owner: {change['owner']}")
            self.write = self.writer_factory(dtc, addr, False)
            if self.is_buffer and self.dtcode in self.NUM_LJC_TYPES:
                self.write_arr = self.writer_factory(dtc, addr, True)
        else:
            self.write = None

    def __repr__(self):
        sret = f"LabjackRegister:\n" \
               f"  name:       {self.name}\n" \
               f"  address:    {self.address}\n" \
               f"  dtype:      {self.dtype} ({self.dtcode})\n" \
               f"  access:     {self.access}" \
               f"  streamable: {self.streamable}\n" \
               f"  is buffer:  {self.is_buffer}\n" \
               f"  uses RAM:   {self.uses_ram}\n"
        if len(self.tags) > 0:
            sret += f"              {self.tags}\n"
        sret += LabjackRegisterBase.format_desc(self.desc)
        return sret


class LabjackRegisterGroup(LabjackRegisterBase):
    """"""
    addresses = trlt.List(
        default_value=[],
        help="Not yet implemented! When supporting non-contiguous "
             "RegisterGroups, an explicit address list will be necessary.")
    address_range = trlt.List(
        default_value=[],
        help="Start (smallest) and end (largest) address in the RegisterGroup.")
    _np1 = trlt.Unicode(
        '', help="1st part to contruct a name from for a single register.")
    _np2 = trlt.Unicode(
        '', help="2nd part to contruct a name from for a single register.")
    _mult = trlt.Int(
        default_value=1,
        help='Data type dpendent multiplier. ')
    _slots_per_item = {
        LJC_UINT16: 1, LJC_UINT32: 2, LJC_INT32: 2, LJC_FLOAT32: 2,
        LJC_BYTE: 1, -1: 1}

    @classmethod
    def reader_factory(cls, dtc: int, addr: int,
                       ln=None, buf: bool = False) -> Callable:
        if dtc in cls.NUM_LJC_TYPES:
            if ln is None:
                raise ValueError(
                    "LabjackRegisterGroup.reader_factory: "
                    "Need a length to create a reader for a RegisterGroup.")
            if buf:
                return lambda handle, lng: \
                    ljm.eReadAddressArray(handle, addr, dtc, lng)
            else:
                return lambda handle: \
                    ljm.eReadAddressArray(handle, addr, dtc, ln)
        else:
            raise ValueError(
                "LabjackRegisterBase.reader_factory: "
                f"Cannot create reader method for a RegisterGroup of "
                f"{cls.DTMAP[dtc]} type registers.")

    @classmethod
    def writer_factory(cls, dtc: int, addr: int,
                       buffer: bool = False) -> Callable:
        if dtc in cls.NUM_LJC_TYPES:
            return lambda handle, val: \
                ljm.eWriteAddressArray(handle, addr, dtc, len(val), val)
        elif dtc == LJC_BYTE:
            raise ValueError(
                "LabjackRegisterBase.writer_factory: "
                "Cannot create writer method for a group of byte registers.")
        elif dtc == LJC_STRING:
            raise ValueError(
                "LabjackRegisterBase.writer_factory: "
                "Cannot create writer method for a group of string registers.")

    def _range_from_name(self, name: str) -> tuple:
        if "#" in name:
            name, rem = name.split('#(', 1)
            start, rem = rem.split(':', 1)
            try:
                end, rem = rem.split(')', 1)
            except ValueError as err:
                print(f"LabjackRegisterGroup._range_from_name: failed to parse "
                      f"name: '{name}'. rem: {rem}", file=sys.stderr)
                raise err
            self._np1, self._np2 = name, rem
            start = self.address + self._mult * int(start)
            end = self.address + self._mult * int(end)
            return start, end
        else:
            raise ValueError(f"Cannot make a RegisterGroup from name {name}.")

    @trlt.validate('name')
    def _validate_name(self, proposal):
        ret = proposal['value']
        if len(ret) > ljc.MAX_NAME_SIZE:
            raise trlt.TraitError(
                f"Register names on this modbus system cannot be longer than "
                f"{ljc.MAX_NAME_SIZE} characters. Got a request to set a name "
                f"with {len(ret)} characters.")
        return ret

    @trlt.validate('address_range')
    def _validate_address_range(self, proposal):
        start, end = proposal['value']
        rem = (end - start) % self._mult
        if rem != 0:
            warn(f"Cannot apply this address range: {(start, end)}, "
                 f"mult: {self._mult}, name: {self.name}")
        return start, end

    @trlt.observe('name', 'address', 'address_range', 'access', 'dtype')
    def _name_or_address_changed(self, change):
        nm = change['name']
        addr, acc, dtc = self.address, self.access, self.dtcode
        if nm == 'dtype':
            dtc = self.DTMAP[change['new']]
            self._mult = self._slots_per_item[dtc]
            self.address_range = self._range_from_name(self.name)
        elif nm == 'name':
            self.address_range = self._range_from_name(change['new'])
        elif nm == 'address':
            addr = change['new']
            self.address_range = self._range_from_name(self.name)
        elif nm == 'access':
            acc = change['new']
        elif nm == 'address_range':
            rng = change['new']
            np1, np2 = self._np1, self._np2
            start, end = rng
            s = (start - addr) // self._mult
            e = (end - addr) // self._mult
            self.name = f"{np1}#({s}:{e}){np2}"
        if 'r' in acc:
            self.read = self.reader_factory(
                dtc, addr, len(self), self.is_buffer)
        else:
            self.read = None
        if 'w' in acc:
            self.write = self.writer_factory(dtc, addr, self.is_buffer)

    def __len__(self):
        try:
            start, end = self.address_range
            return (end - start) // self._mult + 1
        except Exception as err:
            err.args += (f"Cannot determine length of this RegisterGroup: "
                         f"{self.address_range=}, {self.name=}.", )
            raise

    def __getitem__(self, item):
        name = self.name
        np1, np2 = self._np1, self._np2
        if isinstance(item, int):
            assert item < len(self), \
                f"Item {item} out of allowed range for itemized access: " \
                f"{name}, {self.address_range}, length: {len(self)}."
            name = np1 + str(item) + np2
            addr = self.address + self._slots_per_item[self.dtcode] * item
            ret = LabjackRegister(name=name, address=addr, dtype=self.dtype)
            for traitname, trait in self.traits().items():
                if traitname not in ['name', 'address', 'dtype']:
                    val = self.__getattribute__(traitname)
                    if val != trait.default_value:
                        ret.__setattr__(traitname, val)
        elif isinstance(item, slice):
            start, stop = item.start, item.stop
            start = 0 if start is None else start
            stop = len(self) if stop is None else stop
            assert start >= 0 and \
                   stop <= len(self), \
                   f"Wrong item range {item} for RegisterGroup with range " \
                   f"{self.address_range}"
            if item.step is not None:
                if item.step != 1:
                    raise NotImplementedError(
                        "Register range must be continuous.")
            addr = self.address
            ret = LabjackRegisterGroup(
                name=name, address=addr + start,
                address_range=[addr + start, addr + stop])
            for traitname, trait in self.traits().items():
                if traitname not in ['name', 'address', 'address_range']:
                    val = self.__getattribute__(traitname)
                    if val != trait.default_value:
                        ret.__setattr__(traitname, val)
        else:
            raise ValueError(
                "LabjackRegisterGroup.__getitem__: item must be int or slice, "
                f"but I got {item}.")
        return ret

    def __repr__(self):
        start, end = self.address_range
        sret = f"LabjackRegisterGroup:\n" \
               f"  name:       {self.name}\n" \
               f"  address:    {self.address}\n" \
               f"  range:      length {len(self)} ({start} ... {end})\n" \
               f"  dtype:      {self.dtype}  (LJM code {self.dtcode}, " \
               f"{self._mult} slots per address)\n" \
               f"  access:     {self.access}\n" \
               f"  streamable: {self.streamable}\n" \
               f"  is buffer:  {self.is_buffer}\n" \
               f"  uses RAM:   {self.uses_ram}\n"
        if len(self.tags) > 0:
            sret += f"              {self.tags}\n"
        sret += LabjackRegisterBase.format_desc(self.desc)
        return sret


class LabjackRegistry:
    """"""
    ljm_constants_keys = [
        'address', 'name', 'type', 'readwrite', 'description'
    ]
    ljm_traits_map = {
        'address': 'address', 'name': 'name', 'type': 'dtype',
        'readwrite': 'access', 'streamable': 'streamable',
        'altnames': 'altnames', 'constants': 'constants', 'default': 'default',
        'usesRam': 'uses_ram', 'isBuffer': 'is_buffer', 'description': 'desc'
    }

    def __init__(self, model: str):
        self.model: str = model
        self.items: dict = {}

    @property
    def names(self) -> list:
        return list(self.items.keys())

    def load_from_dct(self, dct: dict) \
            -> Union[LabjackRegister, LabjackRegisterGroup]:
        try:
            name = dct['name']
        except KeyError as err:
            err.args += (f"Cannot find key 'name' in {dct}",)
            raise err
        for k in self.ljm_constants_keys:
            assert k in dct.keys(), \
                f"Cannot initialize register (group) '{name}'. " \
                f"Key '{k}' is missing."
        initdct = {}
        for k in self.ljm_traits_map.keys():
            try:
                initdct[self.ljm_traits_map[k]] = dct[k]
            except KeyError:
                pass
        if "#" not in name:
            ret = LabjackRegister(**initdct)
        else:
            ret = LabjackRegisterGroup(**initdct)
        return ret

    def __getitem__(self, item: str) \
            -> Union[LabjackRegister, LabjackRegisterGroup]:
        names = self.names
        if item in names:
            return self.items[item]
        else:
            if "#" in item:
                np1, rem = item.split('#(', 1)
                start, rem = rem.split(":", 1)
                end, rem = rem.split(")", 1)
                start, end = int(start), int(end)
                for name in names:
                    if name.startswith(np1) and name.endswith(rem):
                        grp = self.items[name]
                        return grp[start:end]
                raise ValueError(f"Cannot find or makeup {item}.")
            else:
                pos = re.search(r'\d+', item)
                if pos is not None:
                    np1, np2 = item[:pos.start()],  item[pos.end():]
                    ch = int(pos.group())
                    for name in names:
                        if name.startswith(np1) and name.endswith(np2):
                            itm = self.items[name]
                            # print(f"LabjackRegistry.__getitem__: "
                            #       f"item: {item}")
                            return itm[ch] if "#" in itm.name else itm
                    raise KeyError(f"Cannot find '{item}'. pos: {pos}")
                else:
                    raise KeyError(f"Cannot find '{item}'.")

    def __getattr__(self, item):
        if item == '__len__':
            return self.__len__()
        if item in self.__dict__.keys():
            return self.__dict__[item]
        else:
            try:
                return self[item]
            except KeyError:
                raise AttributeError(f"No '{item}' to be found here.")


class T7Registers(LabjackRegistry):
    """"""
    _grs = [
        "AIN#(0:254)", "DAC#(0:1)", "FIO#(0:7)", "EIO#(0:7)", "CIO#(0:3)",
        "MIO#(0:2)", "FIO_STATE", "FIO_DIRECTION",
        "LUA_RUN", "LUA_SOURCE_SIZE", "LUA_SOURCE_WRITE",
        "LUA_DEBUG_ENABLE", "LUA_DEBUG_NUM_BYTES", "LUA_DEBUG_DATA",
        "LUA_SAVE_TO_FLASH", "LUA_LOAD_SAVED", "LUA_SAVED_READ_POINTER",
        "LUA_SAVED_READ", "LUA_NO_WARN_TRUNCATION", "LUA_RUN_DEFAULT",
        "LUA_DEBUG_ENABLE", "LUA_DEBUG_ENABLE_DEFAULT",
        "LUA_DEBUG_NUM_BYTES_DEFAULT", "FILE_IO_LUA_SWITCH_FILE",
        "USER_RAM#(0:39)_F32", "USER_RAM#(0:9)_I32", "USER_RAM#(0:39)_U32",
        "USER_RAM#(0:19)_U16", "USER_RAM_FIFO#(0:3)_DATA_U16",
        "USER_RAM_FIFO#(0:3)_DATA_U32", "USER_RAM_FIFO#(0:3)_DATA_I32",
        "USER_RAM_FIFO#(0:3)_DATA_F32", "USER_RAM_FIFO#(0:3)_ALLOCATE_NUM_BYTES",
        "USER_RAM_FIFO#(0:3)_NUM_BYTES_IN_FIFO", "USER_RAM_FIFO#(0:3)_EMPTY",
        "STREAM_SETTLING_US", "STREAM_RESOLUTION_INDEX", "STREAM_TRIGGER_INDEX",
        "TEMPERATURE_DEVICE_K", "DEVICE_NAME_DEFAULT", "SERIAL_NUMBER", "TEST",
        "RTC_TIME_S", "SYSTEM_COUNTER_10KHZ", "RTC_SET_TIME_S"
    ]

    def __init__(self, handle: int = -1):
        super(T7Registers, self).__init__(model='T7')
        self.handle: int = handle
        with open(LJM_CONST_PATH) as fp:
            _json = json.load(fp)
        for dct in _json['registers']:
            if dct['name'] in self._grs:
                try:
                    itm = self.load_from_dct(dct)
                except ValueError:
                    print(f"VE: {dct}")
                    raise
                except trlt.TraitError:
                    print(dct)
                    raise
                self.items[itm.name] = itm


t7regs = T7Registers()


class LabjackT7DebugPort(DebugPort):
    """Reads the output (i.e. print statements) from running lua scripts on
    Labjack T7 devices.
    """
    reg_enable: LabjackRegister = t7regs.LUA_DEBUG_ENABLE
    reg_enable_default: LabjackRegister = t7regs.LUA_DEBUG_ENABLE_DEFAULT
    reg_num_bytes: LabjackRegister = t7regs.LUA_DEBUG_NUM_BYTES
    reg_data: LabjackRegister = t7regs.LUA_DEBUG_DATA

    def __init__(self, hndl: int):
        """

        Parameters
        ----------
        hndl : int
            Device handle. Device should be connected.
        """
        super().__init__(hndl)
        self._enabled = None

    @property
    def enabled(self) -> bool:
        if self._enabled is not None:
            return self._enabled
        else:
            raise ValueError("Device needs to be connected first.")

    @enabled.setter
    def enabled(self, val: bool):
        self.reg_enable.write(self.handle, int(val))
        self._enabled = val

    def __len__(self) -> int:
        """Returns the number of bytes available to read in the Labjack's
        debug buffer.
        """
        if not self.enabled:
            return 0
        else:
            return int(self.reg_num_bytes.read(self.handle))

    def read(self, *pars) -> str:
        ret = self.reg_data.read(self.handle, pars[0])
        return ''.join([el.to_bytes(1, 'big').decode('ascii') for el in ret])


class LabjackT7Backend(DeviceBackend):
    """"""
    MANUF: str = "lj"
    ms: dict = {'deviceType': 'T7'}
    CONNECTION_TYPES: tuple = ("ANY", "USB", "TCP", "ETHERNET", "WIFI")
    ANALOG_IN: tuple = tuple([f"AIN{i}" for i in range(14)])
    ANALOG_OUT: tuple = ("DAC0", "DAC1")
    DIGITAL_IO = tuple([f"FIO{i}" for i in range(8)])
    CHANNELS: tuple = ANALOG_IN + ANALOG_OUT + DIGITAL_IO
    max_conn_tries = trlt.Int(
        default_value=10,
        help="Maximum number of tries to repeat upon failed device connection.",
        config=True
    )
    max_addrss_tries: int = 10
    # Registers:
    reg_dev_name_dflt: LabjackRegister = t7regs.DEVICE_NAME_DEFAULT
    reg_serial: LabjackRegister = t7regs.SERIAL_NUMBER
    reg_rtc_s: LabjackRegister = t7regs.RTC_TIME_S
    reg_rtc_10k: LabjackRegister = t7regs.SYSTEM_COUNTER_10KHZ
    reg_rtc_set_s: LabjackRegister = t7regs.RTC_SET_TIME_S
    reg_test: LabjackRegister = t7regs.TEST
    reg_lua_run: LabjackRegister = t7regs.LUA_RUN
    reg_lua_src_ln: LabjackRegister = t7regs.LUA_SOURCE_SIZE
    reg_lua_src_wrt: LabjackRegister = t7regs.LUA_SOURCE_WRITE
    reg_lua_dbg_enbl: LabjackRegister = t7regs.LUA_DEBUG_ENABLE
    reg_lua_dbg_enbl_dflt: LabjackRegister = t7regs.LUA_DEBUG_ENABLE_DEFAULT

    Dbport = LabjackT7DebugPort

    interval_handle = trlt.Int(
        default_value=-1,
        help="Integer that helps the ljm library identify a timing interval. "
             "This interval is meant for the timing of the main data "
             "acquisition loop.",
        config=True
    )
    previntv_handle = trlt.Int(
        default_value=-1,
        help="Integer that helps the ljm library identify a timing interval. "
             "This interval is meant for the timing of the preview loop.",
        config=True
    )
    addrss = trlt.List()
    stop_timeout = trlt.Float(
        default_value=1.0,
        help="Time the some DeviceBackends will wait for the device to end "
             "IO code running locally.",
        config=True
    )
    lua_code_loc = tpth.Path(
        default_value='',
        help="Source location of the lua file that is supposed to run on the"
             "Labjack. Probably better to use absolute paths for that...",
        config=True
    )
    time_shape = trlt.Int(
        default_value=1,
        help="Number of elements (or columns) used for the Labjack time code."
    )
    time_pos = trlt.Tuple(
        default_value=(0,), help="Column index of the time code(s).", config=True)

    def __init__(self, **kwds):
        super().__init__(**kwds)
        print(f"LabjackT7Backend.__init__: {self.srate=}")

    @trlt.observe('channels')
    def _channels_changed(self, change):
        DeviceBackend._channels_changed(self, change)
        self.addrss, self.dtypes = ljm.nameToAddress(change['new'])

    def connect(self, name: str, serial, conn) -> int:
        assert conn in self.CONNECTION_TYPES, \
            f"Got wrong connection type '{conn}'.\n" \
            f"Must be one of {self.CONNECTION_TYPES}"
        i = 0
        hndl = -1
        while hndl == -1:
            if i >= self.max_conn_tries:
                raise ConnectionError(
                    f"Cannot connect to {name} ({serial}) "
                    f"via {conn}. Something is wrong.")
            try:
                hndl = ljm.openS(deviceType=self.ms['deviceType'],
                                 connectionType=conn, identifier=serial)
                break
            except ljm.LJMError as err:
                if err.errorCode == 1230:
                    raise ConnectionRefusedError(err.args[0])
                print(err, flush=True)
                time.sleep(1)
                i += 1
        self._init_device(hndl, name)
        return hndl

    def _init_device(self, hndl, name: str):
        nm = self.reg_dev_name_dflt.read(hndl).strip()
        serial = self.reg_serial.read(hndl)
        print(f"LabjackT7Backend._init_device({hndl=}, {name=}):")
        print(f"  Found <{self.MANUF} {self.ms['deviceType']} "
              f"(serial: {serial:.0f}) name: '{nm}'>.", end="")
        if name != nm:
            print(f" Changing name to '{name}'.", end="", flush=True)
            self.reg_dev_name_dflt.write(hndl, name)
        print("")
        print("Initializing RTC to 0... ", end="")
        self.reg_rtc_set_s.write(hndl, 0)
        print(f"done. Reading RTC: "
              f"{self.reg_rtc_s.read(hndl):.0f}.{self.reg_rtc_10k.read(hndl):.0f}s.")
        test = int(self.reg_test.read(hndl))
        msg = "OK" if test == 1_122_867 else "NOT OK!"
        print(f'  software test: {test}: {msg}')

    def _load_lua_to_device(self, hndl: int, loc: str):
        # stop a possibly running lua script
        i = 0
        while 1:
            if self.reg_lua_run.read(hndl) == 0:
                break
            elif i >= self.max_addrss_tries:
                raise ljm.LJMError(errorAddress=self.reg_lua_run.address,
                                   errorString="Cannot stop Lua.")
            print("LabjackT7Backend.configure: stopping lua script.")
            self.reg_lua_run.write(hndl, 0)
            time.sleep(.6)
            i += 1
        print(f"Loading script '{loc}'")
        # get the lua source code
        with open(loc, mode='rb') as fh:
            script = fh.read() + b'\0'
        # allocate RAM for source code
        self.reg_lua_src_ln.write(hndl, len(script))
        # load lua code from host to device RAM
        self.reg_lua_src_wrt.write(hndl, script)
        self.reg_lua_dbg_enbl.write(hndl, 1)       # enable debug mode
        self.reg_lua_dbg_enbl_dflt.write(hndl, 1)  # also at labjack boot
        self.reg_lua_run.write(hndl, 1)            # 'compile' and run the script

    def configure(self, hndl):
        for ch in self.channels:
            assert ch in self.CHANNELS, \
                f"LabjacT7Backend.configure: cannot find {ch} in available " \
                f"channels {self.CHANNELS}"
        assert self.interval_handle != -1, \
            f"LabjacT7Backend.configure: Interval handle not set."

    def start(self, hndl: int):
        print(f"LabjackT7Backend.start: srate={self.srate}")
        ljm.startInterval(intervalHandle=self.interval_handle,
                          microseconds=int(1e6 / self.srate))

    def read(self, hndl) -> tuple:
        return ljm.eReadAddressArray(hndl, self.addrss, self.dtypes, self.nchans)

    def stop(self, hndl):
        ljm.cleanInterval(intervalHandle=self.interval_handle)

    def disconnect(self, hndl: int):
        try:
            if self.reg_lua_run.read(hndl):
                print("LabjackT7Backend.disconnect: Stopping running script.")
                self.reg_lua_run.write(hndl, 0)
            ljm.close(hndl)
        except ljm.LJMError as err:
            if err.errorCode == 1224:
                print("LabjackT7Backend.disconnect:")
                print("  already disconnected.")
            else:
                raise err


class LabjackLuaIOBackend(LabjackT7Backend):
    """Backend for the Labjack T7, when used with an on-device lua script.

    Two of the four available FIFOs are used to communicate. FIFO0 is used to
    send data from a T7 to the host. FIFO1 sends commands as integers from the
    host to the device.
    """
    MAX_LUA_RUN_RETRIES = 10

    reg_f0_nbyte: LabjackRegister = t7regs.USER_RAM_FIFO0_NUM_BYTES_IN_FIFO
    reg_f0_data: LabjackRegister = t7regs.USER_RAM_FIFO0_DATA_F32
    reg_f1_alloc: LabjackRegister = t7regs.USER_RAM_FIFO1_ALLOCATE_NUM_BYTES
    reg_f1_nbyte: LabjackRegister = t7regs.USER_RAM_FIFO1_NUM_BYTES_IN_FIFO
    reg_f1_data: LabjackRegister = t7regs.USER_RAM_FIFO1_DATA_U32
    reg_f1_clear: LabjackRegister = t7regs.USER_RAM_FIFO1_EMPTY

    def configure(self, hndl: int):
        LabjackT7Backend.configure(self, hndl)
        loc = self.lua_code_loc
        if loc != '':
            self._load_lua_to_device(hndl, loc)
        # allocate 8 bytes (2 elements) for fifo1
        self.reg_f1_alloc.write(hndl, 8)

    def start(self, hndl: int):
        ljm.startInterval(intervalHandle=self.interval_handle,
                          microseconds=int(1e6 / self.loop_speed))
        self.reg_f1_data.write(hndl, 1)

    def start_preview(self, hndl: int):
        ljm.startInterval(intervalHandle=self.previntv_handle,
                          microseconds=333_333)
        self.reg_f1_data.write(hndl, 2)

    def read(self, hndl) -> list:
        try:
            ret = self.reg_f0_data.read_arr(
                hndl, int(self.reg_f0_nbyte.read(hndl) / 4))
            return ret
        except ljm.LJMError as ex:
            if ex.errorCode == 2343:  # USER_RAM_FIFO_INSUFFICIENT_VALUES
                print("LabJackLuaIOBackend.read: 2343 -- I'm too fast.")
            else:
                raise ex

    def _cleanup_queue(self, hndl: int, intv_h: int):
        n = int(self.reg_f0_nbyte.read(hndl) / 4)
        while n > 0:
            self.reg_f0_data.read_arr(hndl, n)
            ljm.waitForNextInterval(intv_h)
            n = int(self.reg_f0_nbyte.read(hndl) / 4)

    def stop_preview(self, hndl: int):
        self.reg_f1_data.write(hndl, 0)
        self._cleanup_queue(hndl, self.previntv_handle)
        ljm.cleanInterval(self.previntv_handle)

    def stop(self, hndl):
        # send stop signal
        self.reg_f1_data.write(hndl, 0)
        print("LabjackLuaIOBackend.stop: put 0 on fifo1.")
        self._cleanup_queue(hndl, self.interval_handle)
        try:
            ljm.cleanInterval(self.interval_handle)
        except ljm.LJMError as err:
            if err.errorCode == 1318:
                print(f"LabjackLuaIOBackend.stop: ignoring invalid interval "
                      f"handle {self.interval_handle}.", file=sys.stderr)

    def _clenup_intervals(self):
        try:
            ljm.cleanInterval(self.interval_handle)
        except ljm.LJMError as err:
            if err.errorCode == 1318:  # LJME_INVALID_INTERVAL_HANDLE
                try:
                    ljm.cleanInterval(self.previntv_handle)
                except ljm.LJMError as err:
                    if err == 1318:
                        err.args += "Something seems wrong with the interval " \
                                    "handles."
                        raise err
            else:
                raise err


class LabjackT7StreamReadBackend(LabjackT7Backend):
    """"""
    reg_settling_us: LabjackRegister = t7regs.STREAM_SETTLING_US
    reg_resolution_index: LabjackRegister = t7regs.STREAM_RESOLUTION_INDEX
    reg_trigger_index: LabjackRegister = t7regs.STREAM_TRIGGER_INDEX

    def configure(self, hndl):
        for ch in self.channels:
            assert ch in self.CHANNELS, \
                f"LabjacT7Backend.configure: cannot find {ch} in available " \
                f"channels {self.CHANNELS}"
        self._setup_streamed_in(hndl)
        self._set_srate(hndl)

    def _setup_streamed_in(self, hndl: int):
        for i, ch in enumerate(self.channels):
            ljm.eWriteName(hndl, f"AIN{i}_RANGE", 10.0)
        self.reg_settling_us.write(hndl, 0)
        self.reg_resolution_index.write(hndl, 0)
        if self.triggered:
            self.reg_trigger_index.write(hndl, 1)
            self._setup_trigger(hndl)
        self.reg_trigger_index.write(hndl, 0)

    def _setup_trigger(self, hndl):
        raise NotImplementedError()

    def _set_srate(self, hndl: int):
        trigg = self.triggered_acq
        self.triggered = False
        self._setup_streamed_in(hndl)
        self.srate = ljm.eStreamStart(
            hndl,
            scansPerRead=int(self.srate / self.loop_speed),
            numAddresses=len(self.channels),
            aScanList=self.addrss,
            scanRate=self.srate
        )
        ljm.eStreamRead(hndl)
        ljm.eStreamStop(hndl)
        self.triggered_acq = trigg

    def start(self, hndl: int):
        self.srate = ljm.eStreamStart(
            hndl,
            scansPerRead=int(self.srate / 3),
            numAddresses=len(self.channels),
            aScanList=self.addrss,
            scanRate=self.srate
        )

    def read(self, hndl) -> tuple:
        return ljm.eStreamRead(hndl)

    def stop(self, hndl):
        ljm.eStreamStop(hndl)


class LabjackT7(Device):
    reg_temp: LabjackRegister = t7regs.TEMPERATURE_DEVICE_K
    io_modes = {'single_read': LabjackT7Backend, 'lua_io': LabjackLuaIOBackend,
                'stream_read': LabjackT7StreamReadBackend}
    conn_types = ["USB", "ETH"]

    def __init__(self, name: str, serial: int, **kwds):
        super().__init__(name, serial, **kwds)
        self.backend = LabjackT7Backend(**kwds)
        self.handle: int = -1
        self.intv_hndl: int = 0

    @property
    def connection(self) -> str:
        if not self.opened:
            raise ConnectionError(f'{self.name} is not open.')
        else:
            return self.status.connection

    def show_device_status(self):
        self.status.temperature = \
            self.reg_temp.read(self.handle) - 273.15
        print(f'  temperature: {self.status.temperature:0.1f}ºC')

    def disconnect(self):
        self.backend.disconnect(self.handle)
        self.status.connected = False
        self.status.connection = "Unknown"
        self.handle = -1

    def configure(self):
        print(f"LabJackT7.configure: handle = {self.handle}")
        self.backend.configure(self.handle)
        self.status.active_chns = self.backend.channels

    def start(self):
        self.backend.start(self.handle)
        self.intv_hndl = self.backend.interval_handle

    def start_preview(self):
        self.backend.start_preview(self.handle)
        self.intv_hndl = self.backend.previntv_handle

    def stop_preview(self):
        self.backend.stop_preview(self.handle)

    def read(self) -> tuple:
        ret = self.backend.read(self.handle)
        ljm.waitForNextInterval(self.intv_hndl)
        return ret

    def stop(self):
        # print(f"LabjackT7.stop: opened={self.opened}, backend={self.backend}")
        if self.opened:
            self.backend.stop(self.handle)
            # self.intv_hndl = 0

    def __repr__(self):
        print(self.backend)
        s = f"<Device {self.backend.MANUF} {self.backend.ms['deviceType']} " \
            f"({self.serial})"
        s += f" open, handle={self.handle}>" if self.opened else ">"
        return s


if __name__ == "__main__":
    print(t7regs)
