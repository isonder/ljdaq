import traitlets as trlt
from traitlets.config import Configurable
from numpy import nan
import warnings

from ljdaq import IOConf


class DeviceStatus(trlt.HasTraits):
    """"""
    connected = trlt.Bool(False)
    connection = trlt.Unicode('Unknown')
    temperature = trlt.Float(-273.15)
    self_test = trlt.Unicode('FAIL')
    armed = trlt.Bool(False)
    active_chns = trlt.List()
    io_mode = trlt.Unicode('')


class DebugPort:
    """"""

    def __init__(self, handle: int):
        self.handle = handle

    @property
    def enabled(self) -> bool:
        raise NotImplementedError()

    @enabled.setter
    def enabled(self, val: bool):
        raise NotImplementedError()

    def __len__(self) -> int:
        raise NotImplementedError()

    def read(self, *pars) -> bytes:
        raise NotImplementedError()


class DeviceBackend(Configurable):
    """This is a container that provides all necessary i/o methods on a class
    level. The methods have a handle parameter that allows to talk to a specific
    device. A `Device` instance may change its backend depending on its
    configuration.
    """
    MANUF: str = ""
    CONNECTION_TYPES: tuple = ()
    CHANNELS: tuple = ()
    ms = {}
    Dbport = DebugPort

    source_type = trlt.Unicode(
        default_value="list",
        help="Expected type of return data.", config=True
    )
    source_layout = trlt.Unicode(
        default_value="muxed", help="Channel layout order indicator.", config=True)
    hardware_trigger = trlt.Bool(
        default_value=False, help="Whether a hardware trigger is present.")
    triggered_acq = trlt.Bool(
        default_value=False, help="Whether or not acquisition uses a trigger.",
        config=True
    )
    debug = trlt.Bool(
        default_value=False,
        help="Whether to enable debug mode on the acquisition device (if "
             "available)",
        config=True
    )
    conn_type = trlt.Unicode(
        default_value="USB",
        help="How connection to the IO device should be established.",
        config=True
    )
    acq_type = trlt.Unicode(
        default_value="single_read",
        help="Type of acquisition. E.g. single/software timed, or streamed.",
        config=True
    )
    srate = trlt.Float(default_value=nan, help="Sampling rate.", config=True)
    duration = trlt.Float(
        default_value=nan,
        help="Acquisition duration in seconds.", config=True
    )
    loop_speed = trlt.Float(
        default_value=nan,
        help="Speed of acquisition loop (when active). In rounds per second.",
        config=True
    )
    channels = trlt.List(
        default_value=[],
        help="A list of (valid) channels to acquire data from.", config=True)
    grouped_dio = trlt.Bool(
        default_value=False,
        help="Whether digital io channels are read from or written to in "
             "groups using an integer value (such as in case of the FIO_STATE "
             "register).",
        config=True
    )

    def __init__(self, **kwds):
        super().__init__(**kwds)

    def connect(self, name: str, serial, conn):
        raise NotImplementedError()

    def configure(self, hndl):
        raise NotImplementedError()

    def start(self, hndl):
        raise NotImplementedError()

    def start_preview(self, hndl: int):
        raise NotImplementedError()

    def stop_preview(self, hndl: int):
        raise NotImplementedError()

    def stop(self, hndl):
        raise NotImplementedError()

    def disconnect(self, hndl):
        raise NotImplementedError()


# noinspection PyAbstractClass
class SingleReadBackend(DeviceBackend):
    """"""

    @classmethod
    def read(cls, hndl) -> tuple:
        raise NotImplementedError()


class SingleWriteBackend(DeviceBackend):
    """"""

    @classmethod
    def write(cls, hndl, val, cnf):
        raise NotImplementedError()


class SingleReadWriteBackend(DeviceBackend):
    """"""

    @classmethod
    def read(cls, hndl, cnf):
        raise NotImplementedError()

    @classmethod
    def write(cls, hndl, val, cnf):
        raise NotImplementedError()


# noinspection PyAbstractClass
class StreamReadBackend(SingleReadBackend):
    pass


class StreamWriteBackend(SingleWriteBackend):
    pass


class StreamReadWriteBackend(SingleReadWriteBackend):
    pass


class Device(IOConf):

    io_modes = {'stream_read': StreamReadBackend,
                'single_read': SingleReadBackend}
    conn_types: list = ['USB']
    conn_type = trlt.Unicode(
        default_value="USB", help="Connection type of device.", config=True)

    def __init__(self, name: str, serial: int, **kwds):
        super().__init__(**kwds)
        self.backend = DeviceBackend(**kwds)
        self.name: str = name
        self.serial: int = serial
        self.status = DeviceStatus()
        self.handle = -1
        self.dbport: DebugPort = self.backend.Dbport(self.handle)

    @property
    def io_mode(self) -> str:
        return self.status.io_mode

    @io_mode.setter
    def io_mode(self, val: str):
        assert val in self.io_modes.keys(), \
            f"Cannot set acq_mode to '{val}'. " \
            f"Must be one of {self.io_modes.keys()}."
        self.status.io_mode = val
        self.backend = self.io_modes[val]
        self.dbport = self.backend.Dbport(self.handle)

    @property
    def opened(self) -> bool:
        return self.status.connected

    @property
    def connection(self) -> str:
        raise NotImplementedError()

    @trlt.observe('conn_type')
    def conn_type_changed(self, change):
        tp = change['new']
        if tp not in self.conn_types:
            warnings.warn(
                f"Device.conn_type_changed: cannot set connection type to '{tp}' "
                f"Must be one of {self.conn_types}. Trying to work with "
                f"{self.conn_types[0]}")
            tp = self.conn_types[0]
        self.conn_type = tp

    def connect(self, conntp: str = "USB"):
        self.handle = self.backend.connect(self.name, self.serial, conntp)
        self.dbport = self.backend.Dbport(self.handle)
        self.dbport.enabled = True
        self.status.connected = True
        return self.handle

    def configure(self):
        raise NotImplementedError()

    def disconnect(self):
        raise NotImplementedError()

    def start(self):
        self.backend.start(self.handle)

    def start_preview(self):
        self.backend.start_preview(self.handle)

    def stop_preview(self):
        raise NotImplementedError()

    def read(self):
        raise NotImplementedError()

    def stop(self):
        raise NotImplementedError()

    def __repr__(self):
        return f"<Device {self.backend.MANUF} ({self.serial})>"
