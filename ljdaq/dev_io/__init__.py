from ljdaq.dev_io.device import (
    Device, DeviceBackend, DebugPort,
    SingleReadBackend, SingleWriteBackend, SingleReadWriteBackend,
    StreamReadBackend, StreamWriteBackend, StreamReadWriteBackend
)
from ljdaq.dev_io.labjack_devio import (
    LabjackT7, LabjackT7Backend, LabjackRegister, LabjackLuaIOBackend,
    LabjackT7StreamReadBackend, LabjackRegisterGroup,
    LabjackRegistry, t7regs
)

# LabJack T7
# t7_00 = LabjackT7(name="GHZ_T7_0", serial=470012431)


class LuaError(Exception):

    err_msg = [
        'n.a.  0',
        'n.a.  1',
        'n.a.  2',
        'n.a.  3',
        'n.a.  4',
        'n.a.  5',
        'n.a.  6',
        'n.a.  7',
        'n.a.  8',
        'n.a.  9',
        'n.a. 10',
        'n.a. 11',
        'n.a. 12',
        'n.a. 13',
        'n.a. 14',
        'n.a. 15'
    ]

    @classmethod
    def decode_err(cls, code: float) -> str:
        assert code <= 2 ** 16 - 1, f"Cannot interpret this error code: {code}."
        ret = []
        for i in range(15, -1, -1):
            j = 2 ** i
            if j < code:
                ret.append(f"error {j: 6d}: {cls.err_msg[i]}")
                code = code - j
        return "\n" + "\n".join(ret)

    def __init__(self, msgcode: float):
        super().__init__(self.decode_err(msgcode))
