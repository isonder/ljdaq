"""File Readers.
"""

import pickle
import zlib
import numpy as np
import pandas as pd
from pathlib import Path
import time
from typing import Union


class FileReader:
    """Reader base object.
    """
    ext: str = ""
    timestamp_format: str = "%Y-%m-%d_%H-%M-%S"
    timestamp_length: int = 19

    def __init__(self, src: Union[Path, str]):
        src = Path(src)
        self.src: Path = src.parent / src.stem if src.suffix == self.ext else src
        self.fhandle = None
        self.opened: bool = False

    @classmethod
    def source_to_fname(cls, src: Path) -> Path:
        return src if src.suffix == cls.ext else Path(f"{src}.{cls.ext}")

    def open(self, src: str):
        raise NotImplementedError()

    def read(self):
        raise NotImplementedError()

    def close(self):
        raise NotImplementedError()


class TextFileReader(FileReader):
    """Read a text file.
    """
    ext = ".txt"
    colsep = ","

    def __init__(self, src: str, chunksize: int = 10000):
        super(TextFileReader, self).__init__(src)
        self.chunksize: int = chunksize

    def open(self, src: Path):
        if src is not None:
            self.src = self.source_to_fname(src)
        self.fhandle = pd.read_csv(self.src + '.' + self.ext,
                                   sep=self.colsep, iterator=True)
        self.opened = True

    def read(self) -> pd.DataFrame:
        yield self.fhandle.get_chunk(self.chunksize)

    def close(self):
        self.fhandle.close()
        self.opened = False


class PickleReader(FileReader):
    """Reader for indexed pickle file.
    """
    ext: str = ".pckl"
    iext: str = ".index"
    np_uint64_bytes: int = 116
    
    def __init__(self, src: Union[str, Path]):
        super(PickleReader, self).__init__(src)
        self.ihandle = None
        self.fpos: np.uint64 = np.uint64(0)

    @classmethod
    def get_sources(cls, fld: Path) -> list:
        assert fld.is_dir(), f"{fld} is not a directory."
        return list(fld.glob(f"*{cls.ext}"))

    @classmethod
    def get_groups(cls, fld: Path) -> dict:
        fpaths = cls.get_sources(fld)
        ret = {}
        for f in fpaths:
            try:
                tspart = f.name[:cls.timestamp_length]
                end = f.name[cls.timestamp_length + 1:]
            except IndexError:
                continue
            # Test if tspart is of a valid time stamp format, and only
            # then append to ret.
            try:
                timestamp = time.strptime(tspart, cls.timestamp_format)
            except ValueError:
                continue
            grp = end.split("_", 2)[0]
            if grp not in ret.keys():
                ret[grp] = []
            ret[grp].append(f)
        return ret

    def open(self, src: Path = None):
        if src is not None:
            self.src = self.source_to_fname(src)
        src = str(self.src)
        self.fhandle = open(src + self.ext, mode='rb')
        self.ihandle = open(src + self.iext, mode='rb')
        self.fpos = pickle.loads(self.ihandle.read(self.np_uint64_bytes))
        self.opened = True

    def read(self):
        while 1:
            try:
                end = pickle.loads(self.ihandle.read(self.np_uint64_bytes))
                if end == 0:  # Don't know why, but the last value in the index
                    break     # file is 0...
            except EOFError:
                break
            try:
                data = pickle.loads(self.fhandle.read(end - self.fpos))[0]
                if len(data) == 0:
                    continue
                yield data
            except IndexError:
                continue
            except EOFError:
                break
            finally:
                self.fpos = end

    def close(self):
        if self.opened:
            self.fhandle.close()
            self.ihandle.close()
            self.opened = False
        else:
            print(f"File '{self.src}' already closed.")


class CompressedPickleReader(PickleReader):
    ext = "pcklz"

    def __init__(self, src: str):
        super(CompressedPickleReader, self).__init__(src)
        self.decomp = zlib.decompressobj()
        self.totlen: int = 0

    def read(self):
        end = pickle.loads(self.ihandle.read(self.np_uint64_bytes))
        print(end, self.fpos, end - self.fpos, flush=True)
        chunk = self.fhandle.read(end - self.fpos)
        uchunk = self.decomp.decompress(chunk) + self.decomp.flush()
        try:
            data = pickle.loads(uchunk)
        except pickle.UnpicklingError as err:
            print(f"end: {end},  fpos: {self.fpos}", flush=True)
            print(f"chunk: {chunk[:20]} ... {chunk[-20:]}", flush=True)
            print(f"uchunk: {uchunk[:20]} ... {uchunk[-20:]}", flush=True)
            raise err
        self.fpos = end
        self.totlen += len(data[0])
        return data[0]

    def readall(self):
        while 1:
            try:
                yield self.read()
            except EOFError:
                break
