from pathlib import Path
from traitlets.config import Configurable, observe
from traitlets import List, Unicode, Dict, Float, Int, Bool
import time


LJM_CONST_PATH = Path(__file__).parent / "dev_io/ljm_constants.json"


class IOConf(Configurable):
    _dch_marks: list = ["dio"]
    _ach_marks: list = ['aio']
    names = List(
        default_value=['time', ''],
        help="Names of acquired channels.", config=True)
    descs = List(
        default_value=[],
        help="Short description for each channel.", config=True)
    timestamp_format = Unicode("%Y-%m-%d_%H-%M-%S", config=True)
    timing = Unicode(
        default_value="n.a.", config=True,
        help="Timing type. E.g. software/hardware, muxed/parallel, no timing "
             "(static) or whatever."
    )
    other_metadata = Dict(
        default_value={},
        help="Any oher metadata as key-value pairs.", config=True
    )
    start_time = Float(
        default_value=time.time(), config=True,
        help="Start time of Reader or Writer. This may be set manually.",
    )
    record_time = Bool(
        default_value=True, config=True,
        help="Whether recorded data contains a time column. If true, don't "
             "forget to check that `time_column` is set correctly.",
    )
    time_column = Int(
        default_value=0,
        help="Index of the time column. Default is -1: invalid.", config=True
    )
    grouped_dio = Bool(
        default_value=False,
        help="Whether digital channels are grouped into one.", config=True
    )
    channels = List(
        default_value=[], config=True,
        help="List of acquisition channel addresses on the device to acquire."
    )

    def __init__(self, **kwds):
        super().__init__(**kwds)
        self._nchans: int = len(self.names)
        if len(self.channels) == 0:
            self.channels = self.names.copy()
        self._analog_channels: list = []
        self._dio_channels: list = []

    @property
    def nchans(self) -> int:
        return self._nchans

    @observe('channels')
    def _channels_changed(self, change):
        self._nchans = len(change['new'])
        diochs, achs = [], []
        for ch in change['new']:
            for mark in self._ach_marks:
                if ch.startswith(mark):
                    achs.append(ch)
            for mark in self._dch_marks:
                if ch.startswith(mark):
                    diochs.append(mark)
        print(
            f"DeviceBackend._channels_changed:\n"
            f"  {self._dch_marks=}\n"
            f"  {change['new']=}\n"
            f"  {achs=}\n"
            f"  {diochs=}"
        )
        self._analog_channels = achs
        self._dio_channels = diochs
